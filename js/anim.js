// let hasScrolled = false;

const tl = anime.timeline({
    easing: 'easeOutSine',
    // duration: 250,
    duration: 200,
    // duration: 150,
    direction: 'alternate',
    // loop: true,

    complete: (anim) => {
        anime({
            targets: '#sonya-baseline',
            opacity: 1,
            // duration: 250,
            duration: 200,
            // duration: 150,
            complete: (anim) => {
                tl2.play()
            }
        })
    }
});

const tl2 = anime.timeline({
    easing: 'easeOutSine',
    // duration: 250,
    duration: 200,
    // duration: 150,
    // direction: 'alternate',
    autoplay: false,
    // loop: true,

    // complete: (anim) => {
    // Also prevent if mobile
    //     if (!hasScrolled) {
    //         setTimeout(() => {
    //             document.querySelector('#intro').scrollIntoView({behavior: "smooth", block: "center", inline: "center"});
    //         }, 1000);
    //     }
    // }
});

/* SCROLL */
/* Cancel scrollIntoView() if scrolltop > 0 */

// if (document.querySelector('html').scrollTop > 0) {
//     hasScrolled = true;
//     console.log(hasScrolled);
// }
// window.onscroll = function (e) {
//     if (document.querySelector('html').scrollTop > 0) {
//         hasScrolled = true;
//         console.log(hasScrolled);
//     }
// }

/* ANIM */

/* tl */

tl
.add({
    targets: '#path-10 path',
    strokeDashoffset: [0, anime.setDashoffset],
})
.add({
    targets: '#path-04 path',
    strokeDashoffset: [anime.setDashoffset, 0],
// }, '-=250')
// }, '-=200')
}, '-=150')
.add({
    targets: '#path-03 path',
    strokeDashoffset: [anime.setDashoffset, 0],
// }, 500)
}, 400)
// }, 300)
.add({
    targets: '#path-02 path',
    strokeDashoffset: [anime.setDashoffset, 0],
// }, 500)
}, 400)
// }, 300)
.add({
    targets: '#path-11 path',
    strokeDashoffset: [anime.setDashoffset, 0],
})
.add({
    targets: '#path-12 path',
    strokeDashoffset: [anime.setDashoffset, 0],
})
.add({
    targets: '#path-01 path',
    strokeDashoffset: [anime.setDashoffset, 0],
// }, 1500)
}, 1000)
// }, 750)
.add({
    targets: '#path-13 path',
    strokeDashoffset: [anime.setDashoffset, 0],
// }, 1500)
}, 1000)
// }, 750)
.add({
    targets: '#path-06 path',
    strokeDashoffset: [anime.setDashoffset, 0],
// }, 2000)
}, 1400)
// }, 1050)
.add({
    targets: '#path-05 path',
    strokeDashoffset: [anime.setDashoffset, 0],
})
.add({
    targets: '#path-07 path',
    strokeDashoffset: [anime.setDashoffset, 0],
// }, 2500)
}, 1800)
// }, 1350)
.add({
    targets: '#path-08 path',
    strokeDashoffset: [anime.setDashoffset, 0],
    // }, 2500)
}, 1800)
// }, 1350)
.add({
    targets: '#path-09 path',
    strokeDashoffset: [anime.setDashoffset, 0],
})

/* tl2 */

tl2
.add({
    targets: '#path-15 path',
    strokeDashoffset: [anime.setDashoffset, 0],
})
.add({
    targets: '#path-14 path',
    strokeDashoffset: [anime.setDashoffset, 0],
// }, 250)
}, 200)
// }, 150)
.add({
    targets: '#path-16 path',
    strokeDashoffset: [anime.setDashoffset, 0],
// }, 250)
}, 200)
// }, 150)
.add({
    targets: '#path-18 path',
    strokeDashoffset: [anime.setDashoffset, 0],
})
.add({
    targets: '#path-17 path',
    strokeDashoffset: [anime.setDashoffset, 0],
})
.add({
    targets: '#path-19 path',
    strokeDashoffset: [anime.setDashoffset, 0],
})
.add({
    targets: '#path-20 path',
    strokeDashoffset: [anime.setDashoffset, 0],
})
