const mobileButton = document.querySelector('#button-mobile');
const menu = document.querySelector('#menu-mobile');

mobileButton.onclick = (e) => {
    menu.classList.toggle('hide');
}

const darkmodeButtons = Array.from(document.querySelectorAll('.darkmode-button'));
const pics = Array.from(document.querySelectorAll('#logo img, .fb-button img, .in-button img, .li-button img, .darkmode-button img, #button-mobile img, .team .website img'));
if (localStorage.getItem('darkmode') === 'true') {
    switchMode(true)
}

darkmodeButtons.forEach((darkmodeButton) => {

    darkmodeButton.onclick = () => {

        switchMode();
    }
})

// Rewrite mailto
// const mailtos = document.querySelectorAll('header nav > ul:first-of-type > li:last-of-type a, #menu-mobile > ul:first-of-type > li:last-of-type a');
// mailtos.forEach((mailto) => {
//     let newMailto = [];
//     newMailto.push(mailto.href.split('sonyapodcast')[0], '@', mailto.href.split('contact')[1], '.com' );
//     newMailto = newMailto.join('');
//     mailto.href = newMailto;
// });

// Change header li size if en
if (window.location.href.includes('en')) {
    const headerItems = document.querySelectorAll('header .desktop:first-of-type > li');
    const itemsIndex = [46, 49, 47, 113];
    for (let i = 0; i < headerItems.length -1; i++) {
        headerItems[i].style.width = itemsIndex[i] + 'px';
    }
}

// About button
const menuButtons = document.querySelectorAll('.menu-button')
menuButtons.forEach((menuButton) => {
    menuButton.onclick = (e) => {
        e.stopPropagation();
        e.target.nextElementSibling.classList.toggle('hide');
        if (e.target.getAttribute('aria-expanded') == 'false') {
            e.target.setAttribute('aria-expanded', 'true');
        }
        else {
            e.target.setAttribute('aria-expanded', 'false');
        }
    }

    menuButton.parentElement.previousElementSibling.firstElementChild.onfocus = () => {
        menuButton.nextElementSibling.classList.add('hide');
        menuButton.setAttribute('aria-expanded', 'false');
    }

    menuButton.parentElement.nextElementSibling.firstElementChild.onfocus = () => {
        menuButton.nextElementSibling.classList.add('hide');
        menuButton.setAttribute('aria-expanded', 'false');
    }

});

window.onclick = (e) => {
    menuButtons.forEach((menuButton) => {
        menuButton.nextElementSibling.classList.add('hide');
        menuButton.setAttribute('aria-expanded', 'false');
    });
}


// UTILS

function switchMode(force) {
    const darkmode = localStorage.getItem('darkmode') === 'true'
    localStorage.setItem('darkmode', force || !darkmode)

    if (force || !darkmode) {
        document.body.classList.add('darkmode');
    }
    else {
        document.body.classList.remove('darkmode');
    }

    pics.forEach((pic) => {
        let src = pic.getAttribute('src');
        if (force || !darkmode) {
            if (!src.includes('2')) {
                src = src.replace('.svg', '2.svg');
            }
        }
        else {
            if (src.includes('2')) {
                src = src.replace('2.svg', '.svg');
            }
        }
        pic.setAttribute('src', src);
    });

}
