// Prevent others breadcrumbs items than hovered/focused item to move
window.addEventListener('load', () => {
    resizeMenu();
});

function resizeMenu() {
    document.querySelectorAll('#breadcrumbs .not-underlined > span').forEach((span) => {
        span.style.width = span.parentElement.offsetWidth + 'px';
        span.style.height = span.parentElement.offsetHeight + 'px';
    });
}
