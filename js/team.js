const buttons = Array.from(document.querySelectorAll('.audio-button'));
const players = [];

buttons.forEach((button) => {
    const player = new Audio();
    player.src = button.dataset.source;
    players.push(player);
    button.onclick = () => {
        players.forEach((audio) => {
            if (!audio.paused) {
                audio.pause();
                player.currentTime = 0;
                player.remove();
                const prevButton = buttons.find(button => button.dataset.source == audio.src.replace(window.location.origin, ''));
                prevButton.disabled = false;
            }
        });
        player.play();
        button.disabled = true;

        player.onended = () => {
            button.disabled = false;
            player.remove();
        }
    }
});
