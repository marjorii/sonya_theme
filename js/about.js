const buttons = document.querySelectorAll('.more');
buttons.forEach((button) => {

    button.onclick = () => {
        button.parentElement.nextElementSibling.classList.toggle('hide');
        if (button.textContent == '+') {
            button.textContent = '-';
            button.setAttribute('aria-expanded', 'true');
        }
        else {
            button.textContent = '+';
            button.setAttribute('aria-expanded', 'false');
        }
    }

    if (window.location.hash) {
        let hash = window.location.hash;
        hash = hash.replace('#', '');
        console.log(button.previousElementSibling, hash);
        if (button.previousElementSibling.id == hash) {
            button.parentElement.nextElementSibling.classList.remove('hide');
            button.textContent = '-';
            button.setAttribute('aria-expanded', 'true');
            button.previousElementSibling.scrollIntoView({behavior: "smooth", block: "center", inline: "center"});
        }
    }

});
