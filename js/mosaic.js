window.addEventListener('load', () => {
    setGridRowHeight();
});

function setGridRowHeight() {
    const gridContainer = document.querySelector('.grid ul');
    const itemWidth = gridContainer.firstElementChild.offsetWidth;
    // gridContainer.style.gridAutoRows = itemWidth + 'px'
    gridContainer.style.setProperty('--width', itemWidth + 'px')
}

window.addEventListener('resize', () => {
    setGridRowHeight();
});
